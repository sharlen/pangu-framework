package com.gitee.pulanos.pangu.framework.generator;

/**
 * 常量表
 * @author xiongchun
 */
public class Constants {

    /**
     * Java数据类型
     */
    public final static class JavaType {

        public final static String DATE = "Date";
        public final static String BIGDECIMAL = "BigDecimal";
        public final static String LONG = "Long";
        public final static String INTEGER = "Integer";
        public final static String BYTE = "byte[]";
    }
}
